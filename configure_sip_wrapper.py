import logging
import os
import sys

logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')

sys.path.insert(0, os.environ['SIP_ROOT'])
sys.path.insert(0, os.environ['PYQT_ROOT'])

import sipconfig
from PyQt5 import QtCore
logging.getLogger(__name__).warning('')
logging.getLogger(__name__).warning('QtCore.PYQT_CONFIGURATION=' + '%s', QtCore.PYQT_CONFIGURATION)
logging.getLogger(__name__).warning('')

sip_filename = sys.argv[1]
sip_makefile = sys.argv[2]

# Generates sip wrapper
sip_config = sipconfig.Configuration()
sip_wrapper_path = sip_filename.replace('.sip', '.sbf')
sip_flags = QtCore.PYQT_CONFIGURATION['sip_flags'].replace(' -x Py_v3', '')
command = os.environ['SIP_ROOT'] + '/sipgen/sip' + ' -c .'
command += ' -b ' + sip_wrapper_path
command += ' -I ' + os.environ['SIP_ROOT'] + '/sipgen'
command += ' -I ' + os.environ['PYQT_ROOT'] + '/sip'
command += ' ' + sip_flags
command += ' ' + sip_filename
logging.getLogger(__name__).warning('command=' + '%s', command)
os.system(command)

# Generates library
extraFlags = '-std=c++11 -I{QT_ROOT}/include -I{QT_ROOT}/include/QtCore -I{QT_ROOT}/include/QtGui -I{SIP_ROOT}/siplib'.format(**os.environ)
target_dir = os.getcwd()
makefile = sipconfig.SIPModuleMakefile(sip_config, sip_wrapper_path, makefile=sip_makefile)
makefile.extra_cflags = [extraFlags]
makefile.extra_cxxflags = [extraFlags]
makefile.extra_lflags = ['-Wl,-R' + target_dir]
makefile.extra_lib_dirs = [target_dir]
makefile.extra_libs = [sip_filename.replace('.sip', '')]
makefile.generate()
